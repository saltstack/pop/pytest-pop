"""
We override assignment so that if you take two types of test hubs, you can assign freely between them:

  mock_hub.sub.mod = fn_hub.sub.mod

This will create a hybrid test hub.

Everything below mock_hub.sub.mod will be generated as fn_hub, everything else using mock_hub.
"""
import pop.contract


def test_function_hub_contracted_assignment(mock_hub, fn_hub):
    mock_hub.pop.sub.add = fn_hub.pop.sub.add

    assert mock_hub.pop.sub.add.hub is mock_hub


def test_mod_assignment(mock_hub, fn_hub):
    mock_hub.pop.testing = fn_hub.pop.testing

    assert mock_hub.pop.testing.__class__ is fn_hub.__class__
    assert mock_hub.pop.testing.fn_hub.hub is mock_hub


def test_sub_assignment(mock_hub, fn_hub):
    mock_hub.pop = fn_hub.pop

    assert mock_hub.pop._lazy_hub() is mock_hub
    assert mock_hub.pop.__class__ is fn_hub.pop.__class__
    assert mock_hub.pop.testing._lazy_hub() is mock_hub
    assert mock_hub.pop.testing.__class__ is mock_hub.pop.__class__


def test_contracted_assignment(mock_hub, hub):
    # contracted works
    mock_hub.pop.sub.add = hub.pop.sub.add
    assert isinstance(mock_hub.pop.sub.add, pop.contract.Contracted)
    assert mock_hub.pop.sub.add.hub is mock_hub


def test_real_hub_assignment(mock_hub, hub):
    mock_hub.pop.sub = hub.pop.sub
    assert isinstance(mock_hub.pop.sub.add, pop.contract.Contracted)
    assert mock_hub.pop.sub.add.ref == hub.pop.sub.add.ref


def test_recursive_real_hub_assignment(mock_hub, hub):
    mock_hub.pop = hub.pop
    assert isinstance(mock_hub.pop.sub.add, pop.contract.Contracted)
    assert mock_hub.pop.sub.add.ref == hub.pop.sub.add.ref
