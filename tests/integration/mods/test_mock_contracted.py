import pytest

import pytest_pop.mods.testing as testing


@staticmethod
@pytest.fixture(scope="module", name="hub")
def my_hub(hub):
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    yield hub


def test_hub_contract(hub):
    assert hub.mods.testing.echo("foo") == "contract foo"


def test_contract_hub_contract(hub):
    m_echo = testing.mock_contracted(None, hub.mods.testing.echo)
    m_echo.func.return_value = "bar"
    assert m_echo("foo") == "contract bar"


def test_contract_hub_getattr(hub):
    assert testing.mock_contracted(None, hub.mods.testing.echo).return_value


def test_contract_hub_module(hub):
    m_echo = testing.mock_contracted(None, hub.mods.testing.echo)
    func_module = hub.mods.testing.echo.func.__module__
    assert m_echo.func.__module__ == func_module


def test_signature(hub):
    m_sig = testing.mock_contracted(None, hub.mods.testing.signature_func)
    assert str(m_sig.signature) == "(hub, param1, param2='default')"


def test_get_arguments(hub):
    m_sig = testing.mock_contracted(None, hub.mods.testing.signature_func)
    m_sig("passed in")


def test_copy_func_attributes(hub):
    echo = testing.mock_contracted(None, hub.mods.testing.echo)
    attr_func = testing.mock_contracted(None, hub.mods.testing.attr_func)

    with pytest.raises(AttributeError):
        assert echo.func.test
    assert attr_func.func.test is True

    with pytest.raises(AttributeError):
        assert echo.func.__test__
    assert attr_func.func.__test__ is True
